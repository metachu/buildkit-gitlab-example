FROM python:3.8.2-slim-buster AS python_base

WORKDIR /code

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1

#-------------------------------------------------------
# Some miscellaneous apt packages we are installing and want to do in parallel
FROM python_base as base

# Essential build tools
RUN  apt-get update \
        && apt-get -yq --no-install-recommends install \
        less git make gpg gpg-agent bsdmainutils curl libnss3 \
	wget  && \
        # build-essential python-dev
	curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -

# Package managers and extra packages
RUN echo "deb https://deb.nodesource.com/node_13.x buster main" > /etc/apt/sources.list.d/nodesource.list \
        && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
        && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
        && apt-get update \
        && apt-get -yq --no-install-recommends install \
	# Random packages required
        nodejs \
        && rm -rf /var/lib/apt/lists/*

#-------------------------------------------------------

FROM mhart/alpine-node:13.13.0 as node_builder
# Node packages (changes less often)
WORKDIR /code
COPY package.json yarn.lock /code/
RUN yarn install

#-------------------------------------------------------
FROM python_base as python_builder
RUN pip install poetry

COPY pyproject.toml poetry.lock /code/
RUN poetry install --no-interaction --no-ansi -vvv


#-------------------------------------------------------
FROM base as final
# Python packages
COPY --from=NODE_BUILDER /code /code
COPY --from=python_builder /root/.cache /root/.cache

COPY . /code/

ENV PATH="node_modules/.bin/:${PATH}"

CMD ["yarn", "run", "start"]

